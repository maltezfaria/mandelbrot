module Mandelbrot

export step_mandel, is_mandel_8, compute_mandel_8, mandel

@inline function step_mandel(fr,fi,fr2,fi2,cr,ci)
    fi  = 2 .* fr .* fi .+ ci
    fr  = fr2 .- fi2 .+ cr
    fr2 = fr .* fr
    fi2 = fi .* fi
    return fr, fi, fr2, fi2
end

function is_mandel_8(cr,ci)
    fr  = cr
    fi  = ci
    fr2 = cr.*cr
    fi2 = ci.*ci
    f2  = fr2 .+ fi2
    i = 1
    while i < 50
        for _ in 1:7
            fr, fi, fr2, fi2 = step_mandel(fr,fi,fr2,fi2,cr,ci)
            i += 1
        end
        f2 = fr2 .+ fi2
        all(f2 .> 4) && (return 0x00)
    end
    res = 0xff
    f2[1] <= 4.0 || (res &= 0b01111111)
    f2[2] <= 4.0 || (res &= 0b10111111)
    f2[3] <= 4.0 || (res &= 0b11011111)
    f2[4] <= 4.0 || (res &= 0b11101111)
    f2[5] <= 4.0 || (res &= 0b11110111)
    f2[6] <= 4.0 || (res &= 0b11111011)
    f2[7] <= 4.0 || (res &= 0b11111101)
    f2[8] <= 4.0 || (res &= 0b11111110)
    return res
end

function compute_mandel_8(xrange,yrange,N)
    M = Array{UInt8}(undef,div(N,8),N)
    Threads.@threads for j = 1:N
        @inbounds y_vec = ntuple(i->yrange[j],8)
        @simd for i =1:div(N,8)
            @inbounds M[i,j] = is_mandel_8(xrange[i],y_vec)
        end
    end
    return M
end

function mandel(io::IO,N)
    x0     = ntuple(i -> -1.5 + (i-1)*2/N,8)
    xrange = [x0 .+ (k-1)*2*8/N for k=1:div(N,8)]
    yrange = range(-1.0,1.0-2/N,length=N)
    M  = compute_mandel_8(xrange,yrange,N)
    write(io, "P4\n$N $N\n")
    write(io, M)
end
mandel(n) = mandel(stdout,n)

end # module
