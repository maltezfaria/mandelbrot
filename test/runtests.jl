using Test, BenchmarkTools
using Mandelbrot: mandel

@testset "Reference solution" begin
    N = 200
    name = "mandel_test.ppm"
    io = open(name,"w")
    mandel(io,N)
    close(io)
    cmd = Cmd(`cmp $name mandel_ref.ppm`,ignorestatus=true)
    out = run(cmd)
    @test out.exitcode == 0
end
